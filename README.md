# Fasterize extension user guide

The official Fasterize extension for Magento aims to improve website speed as well as compatibility with the Fasterize engine. The extension also allows it's user to clean the Fasterize cache directly from the Magento Dashboard.

## Features
* Manual purge of the Fasterize cache on the Magento dashboard
* Automatic purge of the Fasterize cache via events
* Disables the front preload to ensure better compatibility with the Fasterize engine
* Force enable the Bundle JS to ensure better compatibility with the Fasterize engine

## Requirements

Magento Open Source (CE) 2.3 an newer.

Adobe Commerce using on-prem (EE) 2.3 and newer.

Adobe Commerce on Cloud (ECE) 2.3 and newer.

## Installation

### Composer
The preferred way of installing `fasterize/magento2` is through Composer. Simply add `fasterize/magento2`
as a dependency:

```
composer require fasterize/magento2
```

Optional you can download the latest version [here](https://gitlab.com/fasterize/magento2) and install the
decompressed code in your projects directory under *app/code/Fasterize/Magento2*.

## Post-Install

After the installment of the module source code, the module has to be enabled by the *Magento® 2* CLI.

```
bin/magento module:enable Fasterize_Magento2
```

## System Upgrade

After enabling the module, the *Magento® 2* system must be upgraded.

If the system mode is set to *production*, run the *compile* command first. This is not necessary for the *developer* mode.

```
bin/magento setup:di:compile
```

To upgrade the system, the *upgrade* command must be run.

```
bin/magento setup:upgrade
```

## Clear Cache

At last, the *Magento® 2* should be cleared by running the *flush* command.

```
bin/magento cache:flush
```

Sometimes, other cache systems or services must be restarted first, e.g. Apache Webserver and PHP FPM.


# User Guide

## API Configuration

Some feature of the extension use the Fasterize API to properly work (i.e cache purge). The configuration panel is avalaible in the Magento 2 dashboard at Stores > Configuration > Fasterize.

You can get your API information on the Fasterize dashboard.

![api](doc/images/api.png)

## Manual cache purge

The extension provides a way to manually purge the Fasterize cache via the Magento 2 dashboard. You can purge the stores and a specific one if needed.

![manual purge](doc/images/cache.png)

## Automatic cache purge

The extension will automatically flush the Fasterize cache when these Magento 2 events occurs:

```bash
clean_media_cache_after
clean_catalog_images_cache_after
assign_theme_to_stores_after
adminhtml_cache_refresh_type
adminhtml_cache_flush_all
```

![manual purge](doc/images/manual_purge.png)
## Javascript

By default, the extension will force enable the Magento Bundle JS, this can be changed in the settings under Stores > Configuration > Fasterize.

## Fonts

By default, the extension will disable font preloading, this can be changed in the settings under Stores > Configuration > Fasterize.

![settings](doc/images/settings.png)

## Logs

Every request to the Fasterize API are logged and can be found under var/log/fasterize/magento2.log

## Permission

Each configurable feature has it's own ACL permission.

![permission](doc/images/permission.png)

## Contribution
Feel free to contribute to this module by reporting issues or create some pull requests for improvements.

## License
The **Fasterize** Module for *Magento® 2* is released under the MIT License.
