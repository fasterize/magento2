<?php
namespace Fasterize\Magento2\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Config
 * scope config data.
 */
class Config
{
    /** @var string */
    const XML_PATH_FASTERIZE_API_ID = 'fasterize_magento2/api/id';

    /** @var string */
    const XML_PATH_FASTERIZE_API_TOKEN = 'fasterize_magento2/api/token';

    /** @var string */
    const XML_PATH_FASTERIZE_CACHE_MANUAL = 'fasterize_magento2/cache/manual';

    /** @var string */
    const XML_PATH_FASTERIZE_CACHE_AUTO = 'fasterize_magento2/cache/auto';

    /** @var string */
    const XML_PATH_FASTERIZE_SETTINGS_MERGE = 'fasterize_magento2/settings/merge';

    /** @var string */
    const XML_PATH_FASTERIZE_SETTINGS_PRELOAD = 'fasterize_magento2/settings/preload';

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var EncryptorInterface
     */
    private $encryptor;

    /**
     * Config constructor.
     *
     * @param ScopeConfigInterface $scopeConfig
     * @param EncryptorInterface   $encryptor
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        EncryptorInterface $encryptor
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->encryptor = $encryptor;
    }

    /**
     * Returns the value of the manual cache purging settings
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isManualCachePurgingActive($storeId = 0)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_FASTERIZE_CACHE_MANUAL,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Returns the value of the automatic cache purging settings
     *
     * @param int $storeId
     *
     * @return bool
     */
    public function isAutoCachePurgingActive($storeId = 0)
    {
        return (bool) $this->scopeConfig->getValue(
            self::XML_PATH_FASTERIZE_CACHE_AUTO,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Returns the value of the API URL settings
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getApiUrl($storeId = 0)
    {
        return "https://api.fasterize.com/v1/configs/";
    }

    /**
     * Returns the value of the API ID settings
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getApiId($storeId = 0)
    {
        return (string) $this->scopeConfig->getValue(
            self::XML_PATH_FASTERIZE_API_ID,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * Returns the value of the API token settings
     *
     * @param int $storeId
     *
     * @return string
     */
    public function getApiToken($storeId = 0)
    {
        $token = (string) $this->scopeConfig->getValue(
            self::XML_PATH_FASTERIZE_API_TOKEN,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );

        return $this->encryptor->decrypt($token);
    }

    /**
     * Returns the value of the fonts preload settings
     *
     * @return bool
     */
    public function isFontPreloadActive()
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_FASTERIZE_SETTINGS_PRELOAD);
    }

    /**
     * Returns the value of theforce javascript bundle settings
     *
     * @return bool
     */
    public function isForceBundleActive()
    {
        return (bool) $this->scopeConfig->getValue(self::XML_PATH_FASTERIZE_SETTINGS_MERGE);
    }
}
