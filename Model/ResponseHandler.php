<?php
namespace Fasterize\Magento2\Model;

use Magento\Framework\DataObject;
use Magento\Framework\Message\ManagerInterface;

class ResponseHandler
{
    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * MessageManager constructor.
     *
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ManagerInterface $messageManager
    ) {
        $this->messageManager = $messageManager;
    }

    /**
     * Handles the response from the fasterize API
     *
     * @param DataObject $result
     * @param string     $level
     * @return void
     */
    public function manageResult(DataObject $result, $level = 'addErrorMessage')
    {
        $successResponse = null;
        $errorResponse = null;
        $messages = $result->getData();
        if (null === $messages) {
            return;
        }

        foreach ($messages as $label => $message) {
            $label = \strtoupper($label);
            if (null === $message) {
                if (null === $successResponse) {
                    $successResponse = $label;

                    continue;
                }
                $successResponse .= ", {$label}";
            } else {
                if (null === $errorResponse) {
                    $errorResponse = "{$label} {$message}";

                    continue;
                }
                $errorResponse .= ", {$label} {$message}";
            }
        }

        if (null !== $errorResponse) {
            $this->messageManager->{$level}(
                __("An error occurred while clearing the Fasterize cache: {$errorResponse}.")
            );
        }

        if (null !== $successResponse) {
            $this->messageManager->addSuccessMessage(
                __("The Fasterize cache has been cleaned for store view: {$successResponse}.")
            );
        }
    }
}
