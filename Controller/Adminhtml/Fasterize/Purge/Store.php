<?php

namespace Fasterize\Magento2\Controller\Adminhtml\Fasterize\Purge;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\ResponseInterface;
use Fasterize\Magento2\Http\PurgeRequest;
use Fasterize\Magento2\Model\ResponseHandler;

/**
 * Class Store
 * purge by store.
 */
class Store extends Action
{
    /**
     * @var string
     */
    const ADMIN_RESOURCE = 'Fasterize_Magento2::fasterize_cache_management';

    /**
     * @var PurgeRequest
     */
    private $purgeRequest;

    /**
     * @var ResponseHandler
     */
    private $responseHandler;

    /**
     * Store constructor.
     *
     * @param Context         $context
     * @param PurgeRequest    $purgeRequest
     * @param ResponseHandler $responseHandler
     */
    public function __construct(
        Context $context,
        PurgeRequest $purgeRequest,
        ResponseHandler $responseHandler
    ) {
        $this->purgeRequest = $purgeRequest;
        $this->responseHandler = $responseHandler;
        parent::__construct($context);
    }

    /**
     * Purge by store.
     *
     * @return ResponseInterface
     */
    public function execute()
    {
        $storeId = $this->getRequest()->getParam('stores', false);
        $result = $this->purgeRequest->flush($storeId);
        $this->responseHandler->manageResult($result);

        return $this->_redirect('*/cache/index');
    }
}
