# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 1.0.1

### Added

- Nothing

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Fix cache purge using Fasterize API V2

## 1.0.0

### Added

- Fasterize API configuration
- Manual purge of the Fasterize cache on the Magento dashboard
- Automatic purge of the Fasterize cache via events
- Disables the front preload to ensure better compatibility witht the Fasterize engine
- Force enable the Bundle JS to ensure better compatibility witht the Fasterize engine

### Deprecated

- Nothing.

### Removed

- Nothing.

### Fixed

- Nothing
