<?php
namespace Fasterize\Magento2\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Fasterize\Magento2\Http\PurgeRequest;
use Fasterize\Magento2\Model\ResponseHandler;

/**
 * Class FlushAllCacheObserver
 * auto-trigger cache clean.
 */
class FlushAllCacheObserver implements ObserverInterface
{
    /**
     * @var PurgeRequest
     */
    private $purgeRequest;

    /**
     * @var ResponseHandler
     */
    private $responseHandler;

    /**
     * @var bool
     */
    private $purgeFlag;

    /**
     * FlushAllCacheObserver constructor.
     *
     * @param PurgeRequest    $purgeRequest
     * @param ResponseHandler $responseHandler
     */
    public function __construct(
        PurgeRequest $purgeRequest,
        ResponseHandler $responseHandler
    ) {
        $this->purgeRequest = $purgeRequest;
        $this->responseHandler = $responseHandler;
    }

    /**
     * Flush all event. Actions here are not explicitly requested so error will be printed as warning
     *
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if (null === $this->purgeFlag) {
            $result = $this->purgeRequest->flushAll(true);
            if ($result) {
                $this->purgeFlag = true;
                $this->responseHandler->manageResult($result, 'addWarningMessage');
            }
        }
    }
}
