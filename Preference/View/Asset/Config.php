<?php

namespace Fasterize\Magento2\Preference\View\Asset;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config extends \Magento\Framework\View\Asset\Config
{
    /**
     * @var \Fasterize\Magento2\Model\Config
     */
    private $config;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param \Fasterize\Magento2\Model\Config $config
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        \Fasterize\Magento2\Model\Config $config
    ) {
        parent::__construct(
            $scopeConfig
        );
        $this->config = $config;
    }

    /**
     * Check whether bundling of JavaScript files is on
     *
     * @return bool
     */
    public function isBundlingJsFiles()
    {
        if ($this->config->isForceBundleActive()) {
            return true;
        }
        return parent::isBundlingJsFiles();
    }

    /**
     * Check whether merging of JavaScript files is on
     *
     * @return bool
     */
    public function isMergeJsFiles()
    {
        if ($this->config->isForceBundleActive()) {
            return false;
        }
        return parent::isMergeJsFiles();
    }
}
