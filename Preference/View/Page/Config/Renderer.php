<?php

namespace Fasterize\Magento2\Preference\View\Page\Config;

use Magento\Framework\View\Page\Config;
use Magento\Framework\View\Page\Config\Metadata\MsApplicationTileImage;

class Renderer extends \Magento\Framework\View\Page\Config\Renderer
{

    /**
     * @var \Fasterize\Magento2\Model\Config
     */
    private $config;

    /**
     * @param Config $pageConfig
     * @param \Magento\Framework\View\Asset\MergeService $assetMergeService
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Psr\Log\LoggerInterface $logger
     * @param MsApplicationTileImage|null $msApplicationTileImage
     * @param \Fasterize\Magento2\Model\Config $config
     */
    public function __construct(
        Config $pageConfig,
        \Magento\Framework\View\Asset\MergeService $assetMergeService,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Framework\Escaper $escaper,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Psr\Log\LoggerInterface $logger,
        MsApplicationTileImage $msApplicationTileImage = null,
        \Fasterize\Magento2\Model\Config $config
    ) {
        parent::__construct(
            $pageConfig,
            $assetMergeService,
            $urlBuilder,
            $escaper,
            $string,
            $logger,
            $msApplicationTileImage
        );
        $this->config = $config;
    }

    /**
     * Possible fonts type
     *
     * @var array
     */
    const FONTS_TYPE = [
    'eot',
    'svg',
    'ttf',
    'woff',
    'woff2',
    ];

    /**
     * Check if file type can be font
     *
     * @param string $type
     * @return bool
     */
    private function canTypeBeFont(string $type): bool
    {
        return in_array($type, self::FONTS_TYPE, true);
    }

    /**
     * Add default attributes
     *
     * @param string $contentType
     * @param string $attributes
     * @return string
     */
    protected function addDefaultAttributes($contentType, $attributes)
    {
        if ($contentType === 'js') {
            return ' type="text/javascript" ' . $attributes;
        }

        if ($contentType === 'css') {
            return ' rel="stylesheet" type="text/css" ' . ($attributes ?: ' media="all"');
        }

        if ($this->canTypeBeFont($contentType) && $this->config->isFontPreloadActive()) {
            return '';
        } elseif ($this->canTypeBeFont($contentType) && !$this->config->isFontPreloadActive()) {
            return 'rel="preload" as="font" crossorigin="anonymous"';
        }

        return $attributes;
    }
}
