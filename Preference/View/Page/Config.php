<?php

namespace Fasterize\Magento2\Preference\View\Page;

use Magento\Framework\App;
use Magento\Framework\App\Area;
use Magento\Framework\View;
use Magento\Framework\Escaper;
use Magento\Framework\App\ObjectManager;

class Config extends \Magento\Framework\View\Page\Config
{
    /**
     * Constant body attribute page type
     */
    const BODY_ATTRIBUTE_PAGE_TYPE = 'data-frz-page-type';

    /**
     * Add CSS class to page body tag
     *
     * @param string $className
     * @return $this
     */
    public function addBodyClass($className)
    {
        $className = preg_replace('#[^a-z0-9-_]+#', '-', strtolower($className));
        switch ($className) {
            case "cms-index-index":
                $this->addPageType("index");
                break;
            case "checkout-cart-index":
                $this->addPageType("cart");
                break;
            case "checkout-index-index":
                $this->addPageType("checkout");
                break;
            case "catalog-product-view":
                $this->addPageType("product");
                break;
            case "catalog-category-view":
                $this->addPageType("category");
                break;
        }
        $bodyClasses = $this->getElementAttribute(self::ELEMENT_TYPE_BODY, self::BODY_ATTRIBUTE_CLASS);
        // @phpstan-ignore-next-line
        $bodyClasses = $bodyClasses ? explode(' ', $bodyClasses) : [];
        $bodyClasses[] = $className;
        $bodyClasses = array_unique($bodyClasses);
        $this->setElementAttribute(
            self::ELEMENT_TYPE_BODY,
            self::BODY_ATTRIBUTE_CLASS,
            implode(' ', $bodyClasses)
        );
        return $this;
    }

    /**
     * Add page-type class to page body tag
     *
     * @param string $typeName
     *
     * @return $this
     */
    public function addPageType($typeName)
    {
        $this->setElementAttribute(
            self::ELEMENT_TYPE_BODY,
            self::BODY_ATTRIBUTE_PAGE_TYPE,
            $typeName
        );
        return $this;
    }
}
