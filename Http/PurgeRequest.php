<?php

namespace Fasterize\Magento2\Http;

use Magento\Framework\DataObject;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;
use Fasterize\Magento2\Exception\FasterizeException;
use Fasterize\Magento2\Model\Config;
use Laminas\Http\Response;
use Laminas\Http\Request;

/**
 * Class PurgeRequest
 * api request.
 */
class PurgeRequest
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Config
     */
    public $config;

    /**
     * @var DataObject
     */
    private $response;

    /**
     * @var DataObjectFactory
     */
    private $dataObjectFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * RemoteApi constructor.
     *
     * @param SerializerInterface   $serializer
     * @param StoreManagerInterface $storeManager
     * @param Config                $config
     * @param DataObjectFactory     $dataObjectFactory
     * @param LoggerInterface       $logger
     */
    public function __construct(
        SerializerInterface $serializer,
        StoreManagerInterface $storeManager,
        Config $config,
        DataObjectFactory $dataObjectFactory,
        LoggerInterface $logger
    ) {
        $this->serializer = $serializer;
        $this->storeManager = $storeManager;
        $this->config = $config;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->logger = $logger;
    }

    /**
     * Flush the cache of one store
     *
     * @param int $storeId
     *
     * @return null|DataObject
     */
    public function flush($storeId = 0)
    {
        if (null === $this->response) {
            $this->response = $this->dataObjectFactory->create();
        }

        try {
            $storeCode = $this->storeManager->getStore($storeId)->getCode();

            try {
                $this->sendRequest('cache/purge', $storeId);
                $this->response->setData($storeCode);
            } catch (FasterizeException $e) {
                $this->response->setData($storeCode, $e->getMessage());
            }
        } catch (NoSuchEntityException $e) {
            $this->logger->error($e->getMessage());
        }

        return $this->response;
    }

    /**
     * Fmush the cache of all stores
     *
     * @param bool $isAuto = false
     *
     * @return null|DataObject
     */
    public function flushAll($isAuto = false)
    {
        $stores = $this->storeManager->getStores();

        foreach ($stores as $store) {
            $storeId = $store->getId();
            if (!$isAuto || ($this->config->isAutoCachePurgingActive($storeId))) {
                $this->flush($storeId);
            }
        }

        return $this->response;
    }

    /**
     * Wrapper for API calls towards Fasterize service.
     *
     * @param string $service API Endpoint
     * @param int    $storeId API Auth
     * @param string $method  HTTP Method for request
     *
     * @throws FasterizeException
     *
     * @return bool|mixed
     */
    private function sendRequest(
        $service,
        $storeId = 0,
        $method = Request::METHOD_POST
    ) {
        // Validate config
        if (!$this->config->isManualCachePurgingActive($storeId)) {
            throw new FasterizeException(__('Fasterize is disabled in configuration'));
        }
        $apiToken = $this->config->getApiToken($storeId);
        if (!$apiToken) {
            throw new FasterizeException(__('API token is not configured'));
        }
        $apiId = $this->config->getApiId($storeId);
        if (!$apiId) {
            throw new FasterizeException(__('API id is not configured'));
        }
        $uri = "https://api.fasterize.com/v2/configs/" . $apiId . "/{$service}";

        // Client headers
        $headers = [
            "Authorization: {$apiToken}",
            'Accept: application/json',
            'Content-Type: application/json'
        ];
        // Client options
        $options[CURLOPT_CUSTOMREQUEST] = $method;

        $client = new \Laminas\Http\Client();
        $client->setUri($uri);
        $client->setOptions($options);
        $client->setHeaders($headers);
        $client->setMethod($method);
        $client->setRawBody('{}');
    
        $response = $client->send();

        // Parse response
        $responseBody = $response->getBody();
        $responseCode = $response->getStatusCode();
        $responseCodeText = $response->getReasonPhrase();

        $responseLog['method'] = [
            __METHOD__,
        ];
        $responseLog['request'] = [
            'store_id' => $storeId,
            'uri' => $uri,
            'http_method' => $method,
            'headers' => $headers,
        ];
        $responseLog['response'] = [
            'code' => $responseCode,
            'message' => $responseCodeText,
            'body' => $responseBody,
        ];

        // Return error based on response code
        if (200 !== (int) $responseCode) {
            $this->logger->error("Request $method for service {$service}", $responseLog);

            throw new FasterizeException(__("{$responseCode} - {$responseCodeText}"));
        }
        $this->logger->info("Request $method for service {$service}", $responseLog);

        return $this->serializer->unserialize($responseBody);
    }
}
